import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import { Chart } from 'chart.js';
import { AccountDetailService } from '../_services/accountDetail.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  chart = [];
  ctx;
  saldoCuentas;

  constructor(
    private elementRef: ElementRef,
    private accountDetailService: AccountDetailService) {
  }

  ngOnInit() {
    this.getSaldoCuenta();
  }

  getSaldoCuenta() {
    this.accountDetailService.getAccountDetail().subscribe((saldo) => {
      this.saldoCuentas = saldo;
      localStorage.setItem('saldoCuentas', this.saldoCuentas);
      this.ctx = this.elementRef.nativeElement.querySelector('#canvas');
      this.polarArea(this.saldoCuentas);
    });
  }

  polarArea(saldoCuenta) {
    this.chart = new Chart(this.ctx, {
      type: 'polarArea',
      data: {
        datasets: [{
          data: [100000, saldoCuenta, 400000, 90000, 150000],
          backgroundColor: [
            'rgba(255, 99, 132)',
            'rgba(167, 255, 100)',
            'rgba(255, 206, 86)',
            'rgba(75, 192, 192)',
            'rgba(153, 102, 255)',
            'rgba(255, 159, 64)'
          ],

          borderWidth: 0
        }],
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'Ocio',
          'Saldo Disponible',
          'Viajes',
          'Hogar',
          'Estudios'
        ]
      },
      options: {
        legend: {
          position: 'bottom'
        },
        title: {
          display: true,
          text: 'Gastos',
          fontSize: 26,
          padding: 20
        },
        scales: {
          yAxes: [{
            display: false,
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  sendURL(url) {
    window.location.assign(url);
  }
}
