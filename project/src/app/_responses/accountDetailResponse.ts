import { AccountDetail } from '../_entities/accountDetail';

export class AccountDetailResponse {
    accountDetailResponse: AccountDetail[] = [];

    deserializeAll(input: any) {
        Object.assign(this.accountDetailResponse, input);
        return this;
    }
}
