import { CardDetail } from '../_entities/cardDetail';

export class CardDetailResponse {
    cardDetailResponse: CardDetail[] = [];

    deserialize(input: any) {
        Object.assign(this.cardDetailResponse, input);
        return this;
    }
}
