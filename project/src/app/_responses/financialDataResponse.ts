import { FinancialData } from '../_entities/financialData';

export class FinancialDataResponse {
    financialDataResponse: any[] = [];

    deserializeAll(input: any) {
        Object.assign(this.financialDataResponse, input);
        return this;
    }
}
