import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { RespuestaComponent } from './respuesta/respuesta.component';
import { LoginComponent } from './login/login.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { BancosComponent } from './bancos/bancos.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'tutorial', component: TutorialComponent },
    { path: 'bancos/:idBanco', component: BancosComponent },
    { path: 'bancos', component: BancosComponent },
    { path: 'home', component: HomeComponent },
    { path: 'consulta', component: ConsultaComponent },
    { path: 'respuesta/:response', component: RespuestaComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {onSameUrlNavigation: 'reload'}
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
