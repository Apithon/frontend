import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})
export class TutorialComponent implements OnInit {

  slideIndex = 0;
  slideMax = 3;
  categoryColor = '#171717';

  constructor() { }

  ngOnInit() {
    this.categoryColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
    setInterval(() => {
      this.categoryColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
    }, 2500);
  }

  getVisibility(index) {
    return index === this.slideIndex ? 'visible' : 'hidden';
  }

  hasBefore() {
    if (this.slideIndex > 0) {
      return 'visible';
    }
    return 'hidden';
  }

  before() {
    this.slideIndex--;
  }

  next() {
    this.slideIndex++;
    if (this.slideIndex === 3) {
      window.location.assign('/bancos');
    }
  }
}
