import { BaseRequestOptions } from '@angular/http';

export class CustomRequestOptions extends BaseRequestOptions {
    public token: string;

    constructor (
        options?: any
    ) {
        super();

        this.headers.append('Content-Type', 'application/json');

        if (options != null) {
            options.forEach(option => {
                const optionValue = options[option];
                this[option] = optionValue;
            });
        }
    }
}
