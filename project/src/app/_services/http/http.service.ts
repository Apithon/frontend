import { Injectable } from '@angular/core';
import { Observable, of, ObservableInput } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
    Http,
    RequestOptionsArgs,
    Response,
    Headers,
    XHRBackend,
    RequestOptions
} from '@angular/http';
import { environment } from '../../../environments/environment';

import { CustomRequestOptions } from './customRequest.options';
import { LoaderService } from './loader/loader.service';

@Injectable()
export class HttpService extends Http {

    apiUrl = environment.backend;
    prod = environment.production;

    constructor(
        backend: XHRBackend,
        defaultOptions: CustomRequestOptions,
        private loaderService: LoaderService
    ) {
        super(backend, defaultOptions);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        this.showLoader();

        return super.get(this.getFullUrl(url), this.requestOptions(options)).pipe(
            map((res: Response) => {
                this.onSuccess(res);
                try {
                    const obj = res.json();
                    return obj;
                } catch (error) {
                    return res.text();
                }
            }, (error: any) => {
                this.onError(error);
            }),
            catchError(err => this.onCatch(err))
        );
    }

    post(url: string, data?: any, options?: RequestOptionsArgs): Observable<any> {
        this.showLoader();

        return super.post(this.getFullUrl(url), data, this.requestOptions(options)).pipe(
            map((res: Response) => {
                this.onSuccess(res);
                try {
                    const obj = res.json();
                    return obj;
                } catch (error) {
                    return res.text();
                }
            }, (error: any) => {
                this.onError(error);
            }),
            catchError(err => this.onCatch(err))
        );
    }

    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        const _usuario = JSON.parse(localStorage.getItem('usuario'));

        if (options == null) {
            options = new CustomRequestOptions();
        }

        if (options.headers == null) {
            options.headers = new Headers();
        }

        if (_usuario) {
            if (_usuario.token) {
                options.headers.append('Authorization', 'Bearer ' + _usuario.token);
            }
        }

        if (options.params) {
            options.params = options.params.toString();
        }

        return options;
    }

    private getFullUrl(url: string): string {
        return this.apiUrl + url;
    }

    private onCatch(error: any): Observable<any[]> {
        this.onEnd();
        if (!window.navigator.onLine) {
            this.loaderService.alertaMensajeService.mensajeAlerta('Ocurrió un error',
            'Parece que no estás conectado a internet, conectate e intenta de nuevo', 'error');
        }
        if (error.status === 0) {
            this.loaderService.alertaMensajeService.mensajeAlerta('Lo sentimos',
            'Parece que tenemos problemas, intenta de nuevo más tarde', 'error');
        } else {
            return throwError(error);
        }
    }

    private onSuccess(res: Response): void {
        if (!this.prod) {
            console.log('Request successful');
        }
        this.onEnd();
    }

    private onError(res: Response): void {
        if (!this.prod) {
            console.log('Error, status code: ' + res.status);
        }
        this.hideLoader();
    }

    private onEnd(): void {
        this.hideLoader();
    }

    private showLoader(): void {
        this.loaderService.show();
    }

    private hideLoader(): void {
        this.loaderService.hide();
    }
}
