import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs';

import { LoaderState } from './loader';
import { AlertaMensajeService } from '../../alertaMensaje.service';

@Injectable()
export class LoaderService {
    private loaderSubject = new Subject<LoaderState>();

    loaderState = this.loaderSubject.asObservable();

    alertaMensajeService: AlertaMensajeService;

    constructor(
        private _alertaMensajeService: AlertaMensajeService
    ) {
        this.alertaMensajeService = _alertaMensajeService;
    }

    show() {
        this.loaderSubject.next(<LoaderState>{show: true});
    }

    hide() {
        this.loaderSubject.next(<LoaderState>{show: false});
    }
}
