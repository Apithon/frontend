import { Injectable } from '@angular/core';
import swal from 'sweetalert';

@Injectable()
export class AlertaMensajeService {

    constructor() { }

    mensajeAlerta(titulo: string, parrafo: string, evento: string) {
      return swal({
        text: parrafo,
        title: titulo,
        icon: evento,
        buttons: {
          confirm: {
            text: 'Aceptar',
            value: true,
            visible: true,
            className: 'btnSweetConfirmar',
            closeModal: true
          }
        }
      });
    }
}
