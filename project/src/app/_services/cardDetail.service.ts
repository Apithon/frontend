import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http/http.service';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AlertaMensajeService } from './alertaMensaje.service';
import { CardDetail } from '../_entities/cardDetail';
import { CardDetailResponse } from '../_responses/cardDetailResponse';

@Injectable()
export class CardDetailService {
    private cardDetailUrl = environment.cardDetailUrl;
    private listaCardDetail: CardDetailResponse[] = [];
    constructor(
        private http: HttpService,
        private alertaMensajeService: AlertaMensajeService
    ) { }

    getCardDetail() {
        return this.http.get(this.cardDetailUrl).pipe(map((_res: Object) => {
            return new CardDetailResponse().deserialize(_res);
        }), catchError(err => this.onCatch(err)));
    }

    onCatch(error: any): Observable<any[]> {
        if (error.status === 401) {
            this.alertaMensajeService.mensajeAlerta('Lo sentimos', 'Ha ocurrido un error consultando los Card Detail', 'error');
        }
        return null;
    }
}
