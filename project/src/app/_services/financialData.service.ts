import { Injectable} from '@angular/core';
import { HttpService } from './http/http.service';
import { environment } from '../../environments/environment';
import { Observable} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AlertaMensajeService } from './alertaMensaje.service';
import { FinancialDataResponse } from '../_responses/financialDataResponse';
import { PromedioXMes } from '../_entities/promedioXMes';

@Injectable()
export class FinancialDataService {
    private financialDataUrl = environment.financialDataUrl;
    private listaPromedioSemestral: PromedioXMes[] = [];
    constructor(
        private http: HttpService,
        private alertaMensajeService: AlertaMensajeService
    ) { }

    getPromedioSemestral() {
        return this.http.get(this.financialDataUrl).pipe(map((_res: Object) => {
            const res = new FinancialDataResponse().deserializeAll(_res);
            res.financialDataResponse.forEach((financialData) => {
                const obj = financialData.financialData;
                const objEgresoMensual = '' + obj.egress_monthTotal;
                const ojbIngresoMensual = '' + obj.income_monthTotal;
                const promedioXMes = new PromedioXMes();
                promedioXMes.noCuenta = '009099987433';
                promedioXMes.mes = 1;
                promedioXMes.promedioEgreso = +objEgresoMensual.replace('.', '');
                promedioXMes.promedioIngreso = +ojbIngresoMensual.replace('.', '');
                this.listaPromedioSemestral.push(promedioXMes);
                this.pushListaPromedioSemestral(promedioXMes.promedioIngreso, promedioXMes.promedioEgreso);
            });
            return this.listaPromedioSemestral;
        }), catchError(err => this.onCatch(err)));
    }

    pushListaPromedioSemestral(ingresos, egresos) {
        let egresoMensual = egresos;
        let ingresoMensual = ingresos;
        for (let index = 1; index < 6; index++) {
            egresoMensual += 5000;
            ingresoMensual += 900000;
            const promedioXMes = new PromedioXMes();
                promedioXMes.noCuenta = '009099987433';
                promedioXMes.mes = index + 1;
                promedioXMes.promedioEgreso = egresoMensual;
                promedioXMes.promedioIngreso = ingresoMensual;
                this.listaPromedioSemestral.push(promedioXMes);
        }
    }

    onCatch(error: any): Observable<any[]> {
        if (error.status === 401) {
            this.alertaMensajeService.mensajeAlerta('Lo sentimos', 'Ha ocurrido un error consultando los Financial Data', 'error');
        }
        return null;
    }
}
