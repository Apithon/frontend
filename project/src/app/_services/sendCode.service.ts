import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http/http.service';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AlertaMensajeService } from './alertaMensaje.service';

@Injectable()
export class SendCodeService {
    private securityCodeUrl = environment.securityCodeUrl;

    constructor(
        private http: HttpService,
        private alertaMensajeService: AlertaMensajeService
    ) { }

    sendCode(code) {
        return this.http.post(this.securityCodeUrl + '/' + code).pipe(catchError(err => this.onCatch(err)));
    }

    onCatch(error: any): Observable<any[]> {
        if (error.status === 401) {
            this.alertaMensajeService.mensajeAlerta('Oops!', 'Ha ocurrido un error solicitando los permisos de tu banco', 'error');
        }
        return null;
    }
}
