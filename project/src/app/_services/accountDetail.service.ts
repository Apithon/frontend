import { Injectable, Inject } from '@angular/core';
import { HttpService } from './http/http.service';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AlertaMensajeService } from './alertaMensaje.service';
import { AccountDetailResponse } from '../_responses/accountDetailResponse';
import { AccountDetail } from '../_entities/accountDetail';

@Injectable()
export class AccountDetailService {
    private accountDetailDepositUrl = environment.accountDetailDepositUrl;
    private saldoCuentas = 0;
    constructor(
        private http: HttpService,
        private alertaMensajeService: AlertaMensajeService
    ) { }

    getAccountDetail() {
        return this.http.get(this.accountDetailDepositUrl).pipe(map((_res: Object) => {
            const res = new AccountDetailResponse().deserializeAll(_res);
            res.accountDetailResponse.forEach((deposit) => {
                this.saldoCuentas += deposit.cashBalance;
            });
            return this.saldoCuentas;
        }), catchError(err => this.onCatch(err)));
    }

    onCatch(error: any): Observable<any[]> {
        if (error.status === 401) {
            this.alertaMensajeService.mensajeAlerta('Lo sentimos', 'Ha ocurrido un error consultando los Account Detail', 'error');
        }
        return null;
    }
}
