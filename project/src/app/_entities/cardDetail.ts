export class CardDetail {
    card_number: string;
    card_alias: string;
    card_avalaibleBalance: number;
    card_state: string;
    card_payDate: string;
    card_payMin: number;
    card_payTotal: number;
    card_payDollarMin: number;
    card_payDollarTotal: number;
    card_avalaibleAvance: number;
    card_currentPrincipalAmount: number;
}
