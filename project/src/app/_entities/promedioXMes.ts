export class PromedioXMes {
    noCuenta: string;
    mes: number;
    promedioIngreso: number;
    promedioEgreso: number;
}
