export class AccountDetail {
    typeAccount: string;
    numberAccount: string;
    overdraftQuota: number;
    cashBalance: number;
    exchangeBalance: number;
    receivableBalance: number;
    exchangeBalanceStartDay: number;
    cashBalanceStartDay: number;
}
