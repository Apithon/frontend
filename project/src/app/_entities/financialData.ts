export class FinancialData {
    tax_declarant: string;
    income_month: string;
    income_monthOther: string;
    income_monthTotal: string;
    egress_monthTotal: string;
    liabilities_total: string;
    patrimony: string;
    revenue_totalDate: string;
    revenue_Real: string;
    retainer: string;
    taxpayer_class: string;
    taxpayer_type: string;
    currency_code: string;
    iva_regime: string;
    cost_total: string;
}
