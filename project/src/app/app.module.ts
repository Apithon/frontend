import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AlertaMensajeService } from './_services/alertaMensaje.service';
import { AccountDetailService } from './_services/accountDetail.service';
import { FinancialDataService } from './_services/financialData.service';
import { CardDetailService } from './_services/cardDetail.service';
import { AppRoutingModule } from './/app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { RespuestaComponent } from './respuesta/respuesta.component';
import { MenuComponent } from './menu/menu.component';
import { CoreModule } from './_services/http/core.module';
import { LoginComponent } from './login/login.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { BancosComponent } from './bancos/bancos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SendCodeService } from './_services/sendCode.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConsultaComponent,
    RespuestaComponent,
    MenuComponent,
    LoginComponent,
    TutorialComponent,
    BancosComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    AlertaMensajeService,
    AccountDetailService,
    FinancialDataService,
    CardDetailService,
    SendCodeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
