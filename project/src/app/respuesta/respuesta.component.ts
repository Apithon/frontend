import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-respuesta',
  templateUrl: './respuesta.component.html',
  styleUrls: ['./respuesta.component.css']
})
export class RespuestaComponent implements OnInit {

  response: boolean;
  tituloRespuesta;
  subTituloRespuesta;
  tituloPlanAhorro;
  tituloAplazar;
  tituloPreAprobado;

  textoPlanAhorro;
  textoAplazar;
  textoPreAprobado;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((routeParams) => {
      this.response = routeParams.response === 'true';
      console.log(routeParams.response, this.response);
      if (this.response) {
        console.log('true');
        this.tituloRespuesta = environment.tituloRespuestaSi;
        this.subTituloRespuesta = environment.subTituloRespuestaSi;
        this.tituloPlanAhorro = environment.tituloPlanAhorro;
        this.tituloAplazar = environment.tituloDebito;
        this.tituloPreAprobado = environment.tituloCredito;
        this.textoPlanAhorro = environment.textoDebito;
        this.textoAplazar = environment.textoCredito;
        this.textoPreAprobado = environment.textoCombinado;
      } else {
        console.log('false');
        this.tituloRespuesta = environment.tituloRespuestaNo;
        this.subTituloRespuesta = environment.subTituloRespuestaNo;
        this.tituloPlanAhorro = environment.tituloPlanAhorro;
        this.tituloAplazar = environment.tituloAplazar;
        this.tituloPreAprobado = environment.tituloPreAprobado;
        this.textoPlanAhorro = environment.textoPlanAhorro;
        this.textoAplazar = environment.textoAplazar;
        this.textoPreAprobado = environment.textoPreAprobado;
      }
    });
  }

}
