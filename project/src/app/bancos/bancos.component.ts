import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertaMensajeService } from '../_services/alertaMensaje.service';
import { SendCodeService } from '../_services/sendCode.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.css']
})
export class BancosComponent implements OnInit {

  user: any;

  bancolombiaSelected = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alertaMensajeService: AlertaMensajeService,
    private sendCodeService: SendCodeService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('usuario'));
    this.route.params.subscribe(routeParams => {
      let banco = '', token = '';
      banco = routeParams.idBanco;
      if (banco) {
        this.route.queryParams.subscribe(queryParams => {
          token = queryParams.code;
          if (token) {
            this.user['bancolombiaCode'] = token;
            localStorage.setItem('usuario', JSON.stringify(this.user));
            this.setSelects();
          } else {
            this.alertaMensajeService.mensajeAlerta('Oops!', 'Ha ocurrido un error, intenta de nuevo', 'error');
          }
        });
      }
    });
  }

  setSelects() {
    if (this.user['bancolombiaCode']) {
      document.getElementById('list-option-1').setAttribute('checked', 'checked');
      this.bancolombiaSelected = 1;
    }
  }

  bancoSelect(index) {
    if (index === 1) {
      window.location.href = environment.urlBancolombia;
    } else {
      this.alertaMensajeService.mensajeAlerta('Oops!', 'Aún no hemos implementado éste banco', 'error');
    }
  }

  send() {
    const token = this.user['bancolombiaCode'];
    this.sendCodeService.sendCode(token).subscribe(() => {
      window.location.assign('/home');
    });
  }
}
