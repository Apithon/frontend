import { XHRBackend } from '@angular/http';
import { CustomRequestOptions } from '../_services/http/customRequest.options';
import { HttpService } from '../_services/http/http.service';
import { LoaderService } from '../_services/http/loader/loader.service';

function httpServiceFactory(backend: XHRBackend, options: CustomRequestOptions, loaderService: LoaderService ) {
    return new HttpService(backend, options, loaderService);
}

export { httpServiceFactory };
