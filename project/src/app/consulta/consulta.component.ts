import { Component, OnInit } from '@angular/core';
import { FinancialDataService } from '../_services/financialData.service';
import { CardDetailService } from '../_services/cardDetail.service';
import { PromedioXMes } from '../_entities/promedioXMes';
import { CardDetail } from '../_entities/cardDetail';
import { CardDetailResponse } from '../_responses/cardDetailResponse';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  ingresoSemestral = 0;
  promedioIngresoSemestral = 0;
  egresoSemestral = 0;
  promedioEgresoSemestral = 0;
  otrosEgresos = 100000;
  egresosQuince = 0;
  egresosAHoy = 0;
  faltaObligaciones = 0;
  faltaObligacionesSAH = 0;
  nuevoValorDisponible = 0;
  cupoDisponibleTC = 0;
  deudaHoyTC = 0;
  disponibleTC = 0;
  disponibleTA = 0;
  nuevoValorDisponibleSAH = 0;
  promedioEgresoSemestralSIN = 0;
  promedioEgresoSemestralCON = 0;
  private listaPromedioSemestral: PromedioXMes[] = [];
  private listaCardDetail: CardDetail[] = [];
  cardDetail: CardDetail;

  formValorProducto: FormGroup;

  constructor(
    private financialDataService: FinancialDataService,
    private cardDetailService: CardDetailService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.createControls();
  }

  createControls() {
    this.formValorProducto = new FormGroup({
      valor: new FormControl('', [Validators.required]),
    });
  }

  getPromediosSemestral() {
    this.financialDataService.getPromedioSemestral().subscribe((lista) => {
      const formValues = this.formValorProducto.value;
      this.listaPromedioSemestral = lista;
      this.egresosAHoy = lista[0].promedioEgreso;
      this.listaPromedioSemestral.forEach(obj => {
        this.ingresoSemestral += obj.promedioIngreso;
        this.egresoSemestral += obj.promedioEgreso;
      });
      this.promedioIngresoSemestral = this.ingresoSemestral / 6;
      this.promedioEgresoSemestral =  this.egresoSemestral / 6;
      this.promedioEgresoSemestralCON = (this.promedioEgresoSemestral * 1.15) + this.otrosEgresos;
      this.promedioEgresoSemestralSIN = (this.promedioEgresoSemestral) + this.otrosEgresos;
      this.faltaObligaciones = this.promedioEgresoSemestralCON - this.egresosAHoy;
      this.faltaObligacionesSAH = this.promedioEgresoSemestralSIN - this.egresosAHoy;
      this.nuevoValorDisponible = +localStorage.getItem('saldoCuentas') - this.faltaObligaciones;
      this.nuevoValorDisponibleSAH = +localStorage.getItem('saldoCuentas') - this.faltaObligacionesSAH;
      console.log('Ingresos Semestral', this.ingresoSemestral / 6);
      console.log('Egresos Semestral', this.egresoSemestral / 6);
      console.log('Promedio Ingreso: ', this.promedioIngresoSemestral);
      console.log('Promedio Egreso: ', this.promedioEgresoSemestralCON);
      console.log('Promedio Egreso sin ahorro: ', this.promedioEgresoSemestralSIN);
      console.log('Saldo Disponible Hoy: ', localStorage.getItem('saldoCuentas'));
      console.log('Egresos a hoy: ', this.egresosAHoy);
      console.log('Obligaciones que Faltan: ', this.faltaObligaciones);
      console.log('Obligaciones que Faltan SAH: ', this.faltaObligacionesSAH);
      console.log('Nuevo Valor Disponible: ', this.nuevoValorDisponible);
      console.log('Nuevo Valor Disponible sin ahorro: ', this.nuevoValorDisponibleSAH);

      this.cardDetailService.getCardDetail().subscribe((card: CardDetailResponse) => {
        this.listaCardDetail = card.cardDetailResponse;
        this.listaCardDetail.forEach(tar => {
          this.cardDetail = tar;
          this.deudaHoyTC = tar.card_payMin;
          this.cupoDisponibleTC = tar.card_avalaibleBalance;
        });
        this.disponibleTA = +localStorage.getItem('saldoCuentas') - (this.promedioEgresoSemestralCON - this.faltaObligaciones);
        // tslint:disable-next-line:max-line-length
        this.disponibleTC = this.cupoDisponibleTC - this.deudaHoyTC - (this.faltaObligaciones) + +localStorage.getItem('saldoCuentas');
        console.log('Deuda Hoy TC: ', this.deudaHoyTC);
        console.log('Cupo Disponible TC: ', this.cupoDisponibleTC);
        console.log('Disponible TA: ', this.disponibleTA);
        console.log('Disponible TC: ', this.disponibleTC);
        console.log('Valor Producto', formValues.valor);

        if (this.nuevoValorDisponible > formValues.valor) {
          // 'compralo'
          this.router.navigate(['respuesta/true']);
        } else if (this.disponibleTC > formValues.valor) {
          // 'comprelo con su tdc, terminada en xxx'
          this.router.navigate(['respuesta/true']);
        } else if (this.nuevoValorDisponibleSAH >= formValues.valor) {
          // 'comprelo, pero queda pelao'
          this.router.navigate(['respuesta/true']);
        } else {
          // 'llevao del putas'
          this.router.navigate(['respuesta/false']);
        }
      });

    });
  }
}
