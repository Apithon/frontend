// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: 'http://localhost:2000/apithon/',

  // tslint:disable-next-line:max-line-length
  urlBancolombia: 'https://sbapi.bancolombia.com/security/oauth-otp/oauth2/authorize?client_id=ef057f5e-ccb7-417b-8803-765a8fcca894&response_type=code&scope=Deposit-account:read:user%20Card-credit:read:user%20Deposit-account:read:user%20Deposit-account:read:user%20Customer-financial:read:user&redirect_uri=http://apitonvenn.s3-website-us-east-1.amazonaws.com/bancos/bancolombia',

  accountDetailUrl: 'accounts/account-details ',
  accountDetailDepositUrl: 'accounts/deposits',
  financialDataUrl: 'customers/financial-data',
  cardDetailUrl: 'cards/1234/detail',
  securityCodeUrl: 'security',

 // Titulos y textos de las respuestas
    tituloRespuestaSi: '¡ Si Puedes!',
    tituloRespuestaNo: '¡ No te lo recomiendo !',
    subTituloRespuestaSi: 'Juan, puedes realizar tu compra con toda confianza.',
    // tslint:disable-next-line:max-line-length
    subTituloRespuestaNo: 'Juan, en estos momentos no deberías hacer esta compra, actualmente posees unas obligaciones superiores a tus ingresos.',
    tituloPlanAhorro: 'Plan de ahorro',
    tituloAplazar: 'Aplaza tu compra',
    tituloPreAprobado: 'Pre-aprobado',

    tituloDebito: 'Plan Debito',
    tituloCredito: 'Plan Credito',
    tituloCombinado: 'Plan Combinado',

    textoPlanAhorro: 'Es el momento de ahorrar, consulta nuestros productos.',
    textoAplazar: 'Podría ser en 2 meses que terminas una de tus obligaciones.',
    textoPreAprobado: 'Tienes un pre-aprobado de crediágil. Solicítalo aquí.',

    textoDebito: 'Puedes pagarlo con esta tarjeta sin problema',
    textoCredito: 'Puedes pagarlo con tu cupo disponible sin problema',
    textoCombinado: 'Puedes pagar una parte con débito y otra con la de crédito'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
